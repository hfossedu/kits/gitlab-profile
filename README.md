# Kits

Welcome to the HFOSS Kits community.

## Goal

Build repeatable lessons around real HFOSS projects.

## Ground Rules

* Obey and uphold our [Code of Conduct](./code_of_conduct.md)
* When contributing, you understand that ...
  * When you offer a contribution, you are signing off on the [Developer Certificate of Origin](./developer_certificate_of_origin.txt)
  * Your code contributions are licensed under [GPL v3.0 or higher](./licnese_for_code.txt)
  * Your content contributions are licensed under [CC-BY-SA 4.0 or higher](./license_for_content.txt)

## Discord

Join the [Kits Discord Server](https://discord.gg/x2Hn4BFFnW).

Use it to keep up to date, get help, work with others.

## Development Meetings

Our development team meets in the above Discord server. It's agenda and minutes are on the [HFOSSedu/kits group's wiki](https://gitlab.com/groups/hfossedu/kits/-/wikis/home).

## Credits

* Avatar image: "Making community software sustainable" by opensourceway is licensed under CC BY-SA 2.0. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/2.0/?ref=openverse.

## Licenses

Copyright 2023 HFOSSedu Developers

* Content licensed under CC-BY-SA 4.0 International https://creativecommons.org/licenses/by-sa/4.0/
* Software licensed under GPL v3.0 or higher https://www.gnu.org/licenses/gpl-3.0.txt
